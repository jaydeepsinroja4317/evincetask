package com.jd.task.evincetask.ui.activity

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import com.psb.task.evincetask.R
import com.psb.task.evincetask.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var activity: Activity
    private val dropDownArray by lazy { resources.getStringArray(R.array.numbersArray) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        activity = this

        setUpDropDown()
    }

    private fun setUpDropDown() {
        val adapter: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity,
                android.R.layout.simple_spinner_dropdown_item,
                dropDownArray
            )
        binding.spinnerMain.adapter = adapter
        binding.spinnerMain.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                setUpGrid(dropDownArray[p2].toInt())
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }
    }

    private fun setUpGrid(number: Int) {

        val count = binding.layoutConstraint.childCount - 1
        for (i in count downTo 1) {
            binding.layoutConstraint.removeViewAt(i)
        }
        binding.layoutConstraint.invalidate()
        binding.btnRandomGen.isEnabled = true
        val nrr = NoRepeatRandom(1, number * number)
        var textView: TextView
        var lp: ConstraintLayout.LayoutParams
        var id: Int
        val idArray = Array(number) {
            IntArray(
                number
            )
        }
        val cs = ConstraintSet()
        var idCounter = 1

        for (iRow in 0 until number) {
            for (iCol in 0 until number) {
                textView = TextView(this)
                lp = ConstraintLayout.LayoutParams(
                    ConstraintSet.MATCH_CONSTRAINT,
                    ConstraintSet.MATCH_CONSTRAINT
                )
                id = idCounter++
                Log.d("id",id.toString())
                idArray[iRow][iCol] = id
                textView.id = id
                textView.text = id.toString()
                textView.gravity = Gravity.CENTER
                textView.isEnabled = false
                textView.isClickable = false
                textView.setTextColor(ContextCompat.getColor(this, R.color.black))
                textView.background = ContextCompat.getDrawable(this, R.drawable.bg_griditem)
                binding.layoutConstraint.addView(textView, lp)
            }
        }

        cs.clone(binding.layoutConstraint)
        cs.setDimensionRatio(binding.viewGrid.id, "$number:$number")
        for (iRow in 0 until number) {
            for (iCol in 0 until number) {
                id = idArray[iRow][iCol]
                cs.setDimensionRatio(id, "1:1")
                if (iRow == 0) {
                    cs.connect(id, ConstraintSet.TOP, binding.viewGrid.id, ConstraintSet.TOP)
                } else {
                    cs.connect(id, ConstraintSet.TOP, idArray[iRow - 1][0], ConstraintSet.BOTTOM)
                }
            }
            cs.createHorizontalChain(
                binding.viewGrid.id, ConstraintSet.LEFT,
                binding.viewGrid.id, ConstraintSet.RIGHT,
                idArray[iRow], null, ConstraintSet.CHAIN_PACKED
            )
        }

        cs.applyTo(binding.layoutConstraint)
        var random = -1
        binding.btnRandomGen.setOnClickListener {
            random = nrr.getRandom()
            if (random == -1) {
                Toast.makeText(this, "Please generate new grid", Toast.LENGTH_LONG).show()
            } else {
                binding.btnRandomGen.isEnabled = false
                binding.layoutConstraint.getViewById(random)?.let {
                    it.isEnabled = true
                    it.isClickable = true
                    it.setOnClickListener {
                        binding.btnRandomGen.isEnabled = true
                        binding.layoutConstraint.getViewById(random).isClickable = false
                    }
                }
            }
        }


    }
}