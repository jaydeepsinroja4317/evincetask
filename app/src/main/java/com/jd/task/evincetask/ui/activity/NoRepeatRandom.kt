package com.jd.task.evincetask.ui.activity

internal class NoRepeatRandom(minVal: Int, maxVal: Int) {
    private var number: IntArray? = null
    private var N = -1
    private var size = 0

    init {
        N = maxVal - minVal + 1
        number = IntArray(N)
        var n = minVal
        for (i in 0 until N) number!![i] = n++
        size = N
    }

    fun reset() {
        size = N
    }

    // Returns -1 if none left
    fun getRandom(): Int {
        if (size <= 0) return -1
        val index = (size * Math.random()).toInt()
        val randNum = number!![index]

        // Swap current value with current last, so we don't actually
        // have to remove anything, and our list still contains everything
        // if we want to reset
        number!![index] = number!![size - 1]
        number!![--size] = randNum
        return randNum
    }
}
